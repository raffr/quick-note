# Router HSRP packet tracer

virtual ip			-> 192.168.10.1
router 1 ip interface fa0/0	-> 192.168.10.2
router 1 ip interface fa0/0	-> 192.168.10.3


**router 1 (master)**

set ip address

	conf terminal
	interface fa0/0
	ip address 192.168.10.2 255.255.255.0
	no sh

set hsrp

	interface fa0/0
	standby 10 ip 192.168.10.1
	standby 10 priority 150	(main router set higher priority)
	standby 10 preempt

**router 2 (backup)**

set ip address

	conf terminal 
	interface fa0/0
	ip address 192.168.10.3 255.255.255.0
	no sh

set hsrp

	interface fa0/0
	standby 10 ip 192.168.10.1
	standby 10 priority 100 (backup router set lower priority)
	standby 10 preempt

