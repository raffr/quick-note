# Django

create directory for the project

	mkdir myproject
	cd myproject

create virtual environment

	python3 -m venv virtenv
	source virtenv/bin/activate	# run "deactivate" stop the venv

install django

	pip3 install Django

create project with django

	django-admin startproject mywebsite

run django web

	cd mywebsite
	python3 manage.py runserver

 