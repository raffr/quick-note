# Docker
 
install -> docker documentation

give permission user(non-root) to docker

	sudo usermod -aG docker <user>

## Docker configure

docker --help command

	docker --help
	docker <command> --help

download ubuntu image

	docker pull ubuntu

create and run docker container

Run new docker container from docker image.
By default, docker container will exit immediately if
you do not have any task running on the container.
To keep the container running in the background, 
try to run it with --detach (or -d) argument.
 
	docker run -d -t ubuntu

start docker container

	docker start <container-name-or-id>

stop docker container

	docker stop <container-name-or-id>

rename docker container

	docker container rename <container-name-or-id> <newname>

log into docker shell

	docker exec -it <container-name-or-id> /bin/bash

if systemd error, try run it with /etd/init.d or service <service> restart

	service apache2 restart
	/etc/init.d/apache2 restart

remove docker container

	docker rm <container-name-or-id>

show docker proccess

	docker ps		# active docker
	docker ps -a 	# all docker (active/not) are showing

show docker network

	docker network ls

delete docker container

	docker container rm <container-name-or-id>

show docker logs 

	docker logs <container name or id>

specify port

bind the host port "8000" to container port "80"
	
	docker run -d -t -p 8000:80 ubuntu

container name

	docker run -d -t --name example ubuntu

input variable / environment

	docker run -e MYSQL_ROOT_PASSWORD=password1 -d -t mysql

create docker network

	docker network create ubuntu-network

connect container to docker network

	docker -t -d --net ubuntu-network ubuntu

docker build

	docker build -t image-name /path/to/Dockerfile-directory

Dockerfile example

	from 192.168.122.20:5000/python-slim	-> specify image
	run pip3 install Django			-> run linux shell command inside container
	run mkdir /home/app			-> run linux shell command inside container
	copy . /home/app			-> copy current folder files in host os to container directory
	run python3 /home/app/manage.py runserver 0.0.0.0:8000 &	-> run linux shell command inside container

another example (from techworld with anna)

	FROM node			-> install node

	ENV MONGO_DB_USERNAME=admin\	-> set mongodb username
	    MONGO_DB_PWD=password	-> set mongodb password

	RUN mkdir -p /home/app		-> create /home/app folder

	COPY . /home/app		-> copy current folder files to /home/app 

	CMD ["node","server.js"]	-> start the app with:"node server.js"

docker compose

install docker-compose package

	apt install docker-compose

create a file docker-compose.yml

docker-compose.yml example

	version: "3.3"
	services:
	  django-coba:
	    container_name: "django-coba"
	    ports:
	      - "80:8000"
	    image: "192.168.122.20:5000/django-coba"

run the docker-compose.yml in the same directory

	docker compose up

add detach to running in the background

	docker compose up -d

