# Git

## Git automatic deployment

Git documentation define three possible server hooks: 'pre-receive', 
'post-receive' and 'update'. 'Pre-receive' is executed as soon as the server receives a 'push', 
'update' is similar but it executes once for each branch, 
and 'post-receive' is executed when a 'push' is completely finished and it's the one we are interested in.

create bare init, --bare means that our folder will have no source files, just the version control.

	git init --bare web.git

add a file called post-receive

	touch web.git/hooks/**post-receive**
	vi web.git/hooks/**post-receive**

and add the following lines

	#!/bin/bash

	git --work-tree=/home/vm/web-dir --git-dir=/home/vm/web.git checkout -f

'git-dir' is the path to the repository. With 'work-tree', you can define a different path to where your files will actually be transferred to.

The 'post-receive' file will be looked into every time a push is completed and it's saying that your files need to be in /home/vm/web-dir

you can add another bash script if you want

example

	#!/bin/bash

	git --work-tree=/home/vm/web-dir --git-dir=/home/vm/web.git checkout -f
	python3 /home/vm/web-dir/manage.py runserver 0.0.0.0:8000

s