# Haproxy

Installation

    apt install haproxy

configuration file 

    vi /etc/haproxy/haproxy.conf

add configuration at the end of the configuration file

    frontend http_frontend
        bind *:80
        mode http
        default_backend http_backend

    backend http_backend
        balance roundrobin
        server server1 192.16
        server server2_1 192.168.10.10:8080
        server server2_2 192.168.10.10:8081
    
The **balance** setting specifies the load-balancing strategy. In this case, the **roundrobin** strategy is used. This strategy uses each server in turn but allows for weights to be assigned to each server: servers with higher weights are used more frequently. Other strategies include **static-rr**, which is similar to roundrobin but does not allow weights to be adjusted on the fly; and **leastconn**, which will forward requests to the server with the lowest number of connections.
The forwardfor option ensures the forwarded request includes the actual client IP address.

The **server** lines define the actual server nodes and their IP addresses, to which IP addresses will be forwarded. The servers defined here are node1 and node2, each of which will use the health check you have defined.

restart haproxy service

    systemctl restart haproxy