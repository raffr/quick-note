# iscsi target - tgt

install tgt package
    
    apt install tgt

tgt configuration file

    vi /etc/tgt/conf.d/<newfile>

add configuration

    <target tgt.contoh.local:lun1>          # Domain for target
            backing-store /dev/vdb1         # storage space that will be used
            backing-store /dev/vdb2
            initiator-address 192.168.10.12 # Initiator address
    </target>

or

    <target tgt.contoh.local:lun1>          # Domain for target
            direct-store /dev/vdb1          # storage space that will be used
            direct-store /dev/vdb2
            initiator-address 192.168.10.12 # Initiator address
    </target>

restart tgt service

    systemctl restart tgt
