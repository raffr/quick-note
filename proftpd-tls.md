# Fixed proftpd tls connection in debian 11

vi /etc/proftpd/modules.conf

# Install proftpd-mod-crypto to use this module for TLS/SSL support.
LoadModule mod_tls.c

uncomment the LoadModule
and sudo apt install proftpd-mod-crypto
