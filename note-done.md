### Debian Linux

- [x]	lnstalasi server Debian 10.x GNU/Linux

- [x]	Konfigurasi  IP Addressing
    -	ip address & dns configure

- [x]	Konfigurasi firewall Linux
    -	ufw -> allow,deny port

- [x] lnstalasi dan konfigurasi DNS Server
    -	bind9

- [x] lnstalasi dan konfigurasi Web Server (http/https)
    -	apache2 (http/https->openssl)
    -	nginx (http)

- [x] lnstalasi dan konfigurasi Reverse Web Proxy
    -	nginx (http)

- [x] lnstalasi dan konfigurasi Mail Server
    -	webmail -> roundcube, dovecot, postfix, mariadb

- [x] lnstalasi dan konfigurasi FTP Server (ftp/fpts)
    -  	proftpd(http/https->openssl)

- [x] lnstalasi dan konfigurasi SSH Server
    -	openssh

- [x] lnstalasi dan Konfigurasi DHCP Server
    -	isc-dhcp-server --> mikrotik dhcp relay

### Microsoft Windows 10

- [x] lnstalasi Microsoft Windows Client

- [x] Konfigurasi IP dan Network adapter

- [x] Konfigurasi Wireless Network (WiFi )

### Mikrotik Router

- [x] Konfigurasi static route dan dynamic route (OSPF)

- [ ] Konfigurasi Virtual Local Area Network (VLAN)

- [x] Konfigurasi Network Address Translation (NAT)
    -	masquerade

- [x] Konfigurasi Firewall
    -	nat, filter rules

- [x] Konfigurasi DHCP Relay
    -	dhcp server -> debian

- [x] Konfigurasi Access Point (AP)

### Cisco Router dan Cisco Switch Manageable

- [x] Konfigurasi interface (IP address)

- [x] Konfigurasi static route dan dynamic route

- [x] Konfigurasi Virtual Local Area Network (VLAN) dan VTP

- [x] Konfigurasi dynamic route (OSPF)

- [x] Konfigurasi NAT dan Server

